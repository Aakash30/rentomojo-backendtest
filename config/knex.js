const development = require('./index').development;
module.exports = {
  development: {
    client: 'pg',
    useNullAsDefault: true,
    migrations: {
      directory: './../migrations'
    },
    connection: {
      host: development.host,
      user: development.username,
      password: development.password,
      database: development.database
    }
  },
};