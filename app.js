//Recursively crawl popular blogging website https://medium.com using Node.js and harvest all
//possible hyperlinks that belong to medium.com and store them in a database of your choice
const Knex = require("knex");
const { Model } = require("objection");
const KnexConfig = require("./config/knex");
const knex = Knex(KnexConfig.development);
Model.knex(knex);
const rp = require("request-promise");
const cheerio = require("cheerio");
const request = async (url, callback) => {
  rp(url)
    .then( function (html) {
      const $ = cheerio.load(html);
      let length = $("a").length;
      callback({"$":$,"length":length});
    })
    .catch(function (err) {
    });
};

module.exports = request;
