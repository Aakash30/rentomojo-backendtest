const yargs = require("yargs");
const request = require("./app");
const urllib = require("url");
const HyperLinks = require("./Models/HyperLinks");
yargs.version("1.1.0");

yargs
  .command({
    command: "find",
    describe: "Link Crawler",
    builder: {
      url: {
        describe: "Enter the url",
        demandOption: true,
        type: "string",
      },
    },
    handler: function (argv) {
      let url = argv.url;
      request(url, (data) => {
        console.log(argv.url);
        fetchlinks(data.$, data.length, argv.url);
      });
    },
  })
  .parse();

const fetch_meta_links = async (data,url) => {
  for (let i = 0; i < data.length; i++) {
    request(data[i].base_link, (data) => {
      fetchlinks(data.$, data.length,url);
    });
  }
};

const addToDataBase = async (data,url) => {
  if (data.length != 0) {
    try {
      const additem = await HyperLinks.query().insert(data).returning("*");
      await fetch_meta_links(additem,url);
    } catch (err) {}
  }
};

const fetchlinks = async ($, length, url) => {
  let urls = [];
  for (let i = 0; i < length; i++) {
    if ($("a")[i].attribs["href"] != undefined) {
      let count = $("a")[i].attribs["href"].search("medium");
      if (count != -1) {
        //  console.log($('a')[i].attribs["href"][0])
        if ($("a")[i].attribs["href"][0].startsWith("/")) {
          let new_url = url + $("a")[i].attribs["href"];
         
          let merged = {
            base_link: url+urllib.parse(new_url).pathname,
            ...urllib.parse(new_url, true).query,
          };

          urls.push(merged);
        } else {
          let merged = {
            base_link: url+urllib.parse($("a")[i].attribs["href"]).pathname,
            ...urllib.parse($("a")[i].attribs["href"], true).query,
          };
          urls.push(merged);
        }
      }
    }
  }
  console.log(urls);
  addToDataBase(urls,url);
};
