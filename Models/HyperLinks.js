const { Model } = require('objection');
class HyperLinks extends Model {
  static get tableName() {
    return 'baselink_records';
  }
  static get idColumn() {
    return 'id';
  }
  async $beforeInsert() {
    await super.$beforeInsert();
    if (this.base_link) {
      let result = await this.constructor
        .query()
        .skipUndefined()
        .select("id","count")
        .where("base_link", this.base_link)
        .first();
   
      if (result) {
          try{
          
        let data= await this.constructor
        .query().skipUndefined()
        .update({"count":result.count+1}).where("id",result.id)
        
          }
          catch(err)
          {
              console.log(err)
          }
      }
    }}
  }
  module.exports=HyperLinks